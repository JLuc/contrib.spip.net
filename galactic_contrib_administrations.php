<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Installation/maj du plugin.
 *
 * Crée les champs categorie et préfixe pour les rubriques
 *
 * @param string $nom_meta_base_version Nom de la meta dans laquelle sera rangée la version du schéma
 * @param string $version_cible         Version du schéma de données en fin d'upgrade
 *
 * @return void
 */
function galactic_contrib_upgrade(string $nom_meta_base_version, string $version_cible) : void {
	$maj = [];

	// Ajout des colonnes supplémentaires dans les tables spip_rubriques et spip_articles.
	include_spip('inc/cextras');
	include_spip('base/galactic_contrib');
	$maj['1'] = [['spip_org_to_spip_net']];

	// Upgrade 2 et 3 du schéma
	cextras_api_upgrade(galactic_contrib_declarer_champs_extras(), $maj['2']);
	cextras_api_upgrade(galactic_contrib_declarer_champs_extras(), $maj['3']);

	// Suppression de la table des inscriptions et du champ autodoc de spip_articles
	// Fusion avec Contrib Maintenance : on considère que champs extras spécifiques sont déjà intégrés
	$maj['4'] = [
		['sql_drop_table', 'spip_inscriptions'],
		['sql_alter', 'TABLE spip_articles DROP autodoc']
	];

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

/**
 * Désinstalle les données du plugin.
 *
 * Supprime les champs categorie et préfixe pour les rubriques
 *
 * @param string $nom_meta_base_version Nom de la meta dans laquelle sera rangée la version du schéma
 *
 * @return void
 */
function contrib_vider_tables(string $nom_meta_base_version) : void {
	// Suppression des colonnes ajoutées à l'installation.
	include_spip('inc/cextras');
	include_spip('base/galactic_contrib');
	cextras_api_vider_tables(contrib_declarer_champs_extras());

	// on efface la meta de configuration du plugin
	effacer_meta('contrib');

	// on efface la meta du schéma du plugin
	effacer_meta($nom_meta_base_version);
}

/**
 * Parcourt la table spip_document et remplace les liens files.spip.org en files.spip.net
 * et les http://files.spip.net en https://files.spip.net.
 *
 * @return void
**/
function spip_org_to_spip_net() {
	include_spip('inc/sql');
	$urls_problematiques = [
		'http://files.spip.net',
		'https://files.spip.org',
		'http://files.spip.org'
	];

	foreach ($urls_problematiques as $url) {
		$res = sql_select('id_document, fichier', 'spip_documents', 'fichier LIKE ' . sql_quote("{$url}%"));
		while ($raw = sql_fetch($res)) {
			$fichier = str_replace($url, 'https://files.spip.net', $raw['fichier']);
			$id_document = $raw['id_document'];
			sql_updateq('spip_documents', ['fichier' => $fichier], "id_document={$id_document}");
		}
	}
}
