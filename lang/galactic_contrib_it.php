<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/galactic_contrib-contrib_spip_net?lang_cible=it
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'contribution_sincrire' => 'commencer à contribuer', # MODIF

	// D
	'dernieres_modifs' => 'Modifiche recenti', # MODIF
	'derniers_articles' => 'Ultimi articoli'
);
